extends KinematicBody2D

signal laser_shot
signal player_died

const TURN_SPEED = 180
const MOVE_SPEED = 150
const MOVE_ACCELERATION = 0.05
const MOVE_DECELERATION = 0.02
const SCREEN_EDGE = 8
var motion = Vector2(0,0)
var movedir = Vector2(0,0)
var screen_size

var explosion_scene = load("res://scenes/ParticlesPlayerExplosion.tscn")

func _ready():
	screen_size = get_viewport_rect().size
	var main = get_node(Global.NODE_MAIN)
	self.connect("player_died", main, "on_player_died")

func _process(delta):
	if Input.is_action_pressed("ui_left"):
		rotation_degrees -= TURN_SPEED * delta
	if Input.is_action_pressed("ui_right"):
		rotation_degrees += TURN_SPEED * delta
		
	movedir = Vector2(1, 0).rotated(rotation)
	
	if Input.is_action_pressed("ui_up"):
		motion = motion.linear_interpolate(movedir, MOVE_ACCELERATION)
	elif Input.is_action_pressed("ui_down"):
		motion = motion.linear_interpolate(-movedir, MOVE_ACCELERATION)
	else:
		motion = motion.linear_interpolate(Vector2(0,0), MOVE_DECELERATION)
	
	move_and_collide(motion * MOVE_SPEED * delta)
	# stay in screen
	position.x = wrapf(position.x, -SCREEN_EDGE, screen_size.x + SCREEN_EDGE)
	position.y = wrapf(position.y, -SCREEN_EDGE, screen_size.y + SCREEN_EDGE)
	

func _unhandled_input(event):
	if event.is_action_pressed("ui_shoot"):
		$LaserWeapon.shoot(movedir, rotation)
		emit_signal("laser_shot")


func _on_Hitbox_body_shape_entered(body_id, body, body_shape, area_shape):
	if !is_queued_for_deletion() and body.is_in_group("asteroids"):
		# tell everyone player died
		emit_signal("player_died")
		# Explosion particle
		var explosion:Particles2D = explosion_scene.instance()
		explosion.position = global_position
		get_parent().add_child(explosion)
		explosion.emitting = true
		# free the player object
		queue_free()
