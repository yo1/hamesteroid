extends Node2D

func set_score(score: int, at: Vector2):
	global_position = at
	$Label.text = str(score)

func _on_Timer_timeout():
	queue_free()
