extends Node2D

var laser_scene := load("res://scenes/laser.tscn")

func shoot(movedir, rotation):
	var laser = laser_scene.instance()
	
	laser.global_position = self.global_position
	laser.movedir = movedir
	laser.set_sprite_rotation(rotation)
	
	get_node("/root/main").add_child(laser)
