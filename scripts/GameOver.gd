extends Container

func start(is_high_score: bool):
	if is_high_score:
		$NewHighScore.show()
	show()
	$Music.play()
	
func stop():
	hide()
	$Music.stop()
	$FlickerTimer.stop()
	$StartInstructions.hide()
	$NewHighScore.hide()


func _on_FlickerTimer_timeout():
	if $StartInstructions.is_visible_in_tree():
		$StartInstructions.hide()
	else:
		$StartInstructions.show()


func _on_Music_finished():
	$FlickerTimer.start()
