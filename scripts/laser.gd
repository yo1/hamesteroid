extends Area2D

export var movedir := Vector2(1, 0)
var is_sound_finished = false
const SPEED = 500

func set_sprite_rotation(rotation):
	$Sprite.rotate(rotation)
	

func _process(delta):
	position += movedir * delta * SPEED

func _on_VisibilityNotifier2D_viewport_exited(viewport):
	_vanish()


func _on_Laser_body_shape_entered(body_id, body, body_shape, area_shape):
	if body.is_in_group("asteroids"):
		body.explode()
		_vanish()
		
func _vanish():
	if !is_sound_finished:
		yield(get_tree().create_timer(1.0), "timeout")
		
	if !is_queued_for_deletion():
		queue_free()


func _on_AudioStreamPlayer2D_finished():
	is_sound_finished = true
