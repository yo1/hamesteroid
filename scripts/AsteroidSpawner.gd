extends Node2D

var asteroid_scene = load("res://scenes/Asteroid.tscn")
var screen_size
const SPAWN_INTERVAL := 2.0
const DIFFICULTY_INDEX := 1.5

var spawn_interval := SPAWN_INTERVAL
var difficulty_index := DIFFICULTY_INDEX

func _ready():
	screen_size = get_viewport_rect().size

func _spawn_asteroid():
	var asteroid:RigidBody2D = asteroid_scene.instance()
	asteroid.global_position = Vector2(rand_range(10, screen_size.x-10), -10)
	asteroid.angular_velocity = rand_range(-8, 8)
	asteroid.linear_velocity = Vector2(rand_range(-100, 100), rand_range(0, 40))
	add_child(asteroid)

func wipe_all_asteroids():
	for asteroid in get_tree().get_nodes_in_group("asteroids"):
		asteroid.free()

func restart():
	spawn_interval = SPAWN_INTERVAL
	difficulty_index = DIFFICULTY_INDEX
	update_spawn_timer()
	$SpawnTimer.start()
	$DifficultyTimer.start()
	
func stop():
	$SpawnTimer.stop()
	$DifficultyTimer.stop()
	wipe_all_asteroids()

func _on_Timer_timeout():
	_spawn_asteroid()
	
func update_spawn_timer():
	$SpawnTimer.wait_time = float(spawn_interval) / float(difficulty_index)

func _on_DifficultyTimer_timeout():
	difficulty_index += 1
	update_spawn_timer()
	#print($SpawnTimer.wait_time)
	#print(difficulty_index)
