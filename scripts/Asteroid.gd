extends RigidBody2D

var is_exploded = false
var smaller_asteroid_scene
var explosion_particles_scene
onready var score_points_scene = load("res://scenes/ScorePoints.tscn")
var score_points = 100

signal asteroid_explosion
signal update_score
	
func _ready():
	smaller_asteroid_scene = load("res://scenes/AsteroidSmall.tscn")
	explosion_particles_scene = load("res://scenes/ExplosionParticles.tscn")
	
	var camera = get_node(Global.NODE_CAMERA)
	self.connect("asteroid_explosion", camera, "_on_asteroid_explosion")
	
	var main = get_node(Global.NODE_MAIN)
	self.connect("update_score", main, "on_update_score")

func _on_VisibilityNotifier2D_viewport_exited(viewport):
	_free()
	
func explode():
	if is_exploded or is_queued_for_deletion():
		return
		
	is_exploded = true
	
	self.emit_signal("update_score", score_points)
	
	_play_explosion()	
	spawn_smaller_asteroids(rand_range(2, 4))


func spawn_smaller_asteroids(total):
	if !smaller_asteroid_scene:
		return
		
	for i in total:
		var asteroid = smaller_asteroid_scene.instance()
		_init_smaller_asteroid(asteroid)


func _init_smaller_asteroid(asteroid:RigidBody2D):
	asteroid.global_position = self.global_position
	asteroid.linear_velocity = Vector2(rand_range(-300, 300), rand_range(0, 30))
	asteroid.angular_velocity = rand_range(-4, 4)
	get_parent().add_child(asteroid)


func _play_explosion():
	var points_scored = score_points_scene.instance()
	points_scored.set_score(score_points, global_position)
	get_parent().add_child(points_scored)
	
	if $ExplosionSound != null:
		emit_signal("asteroid_explosion")
		self.hide()
		var explosion_particles = explosion_particles_scene.instance()
		explosion_particles.emitting = true
		explosion_particles.global_position = self.global_position
		get_parent().add_child(explosion_particles)
		
		$ExplosionSound.play()
	else:
		_free()


func _on_ExplosionSound_finished():
	_free()


func _free():
	if !is_queued_for_deletion():
		queue_free()
