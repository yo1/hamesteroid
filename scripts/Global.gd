extends Node

const NODE_GUI = "/root/main/GUI"
const NODE_CAMERA = "/root/main/MainCamera"
const NODE_MAIN = "/root/main"

enum GAME_STATE {PLAYING, GAME_OVER, PAUSE}

var game_state = GAME_STATE.GAME_OVER

var high_score = 1000
