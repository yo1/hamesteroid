extends Control

onready var label_score = $MarginContainer/VBoxContainer/HBoxContainer/HBoxContainer/ScoreLabel
onready var label_high_score = $MarginContainer/VBoxContainer/HBoxContainer/HBoxContainer2/HighScoreLabel
onready var game_over = $MarginContainer/GameOver
onready var game_start = $MarginContainer/GameStart

func on_game_over(is_high_score: bool):
	game_start.stop()
	game_over.start(is_high_score)
	
func on_game_start():
	game_over.stop()
	game_start.start()
	
func set_score(score: int):
	label_score.text = str(score)
	
func set_high_score(score: int):
	label_high_score.text = str(score)
