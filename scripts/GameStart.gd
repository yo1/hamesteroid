extends Label

	
func start():
	$Music.play()
	$FlickerTimer.start()
	$StopTimer.start()
	self.show()
	
func stop():
	$Music.stop()
	$FlickerTimer.stop()
	$StopTimer.stop()
	self.hide()

func _on_StopTimer_timeout():
	stop()


func _on_FlickerTimer_timeout():
	if is_visible_in_tree():
		hide()
	else:
		show()
