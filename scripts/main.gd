extends Node2D

var player_scene = load("res://scenes/player.tscn")
var score = 0

func _ready():
	_restart_game()

func on_player_died():
	if Global.game_state != Global.GAME_STATE.PLAYING:
		return
		
	var is_high_score = score > Global.high_score
	Global.game_state = Global.GAME_STATE.PAUSE
	
	if is_high_score:
		Global.high_score = score
		
	$GUI.on_game_over(is_high_score)
	$AsteroidSpawner.stop()
	
	yield(get_tree().create_timer(2.0), "timeout")
	Global.game_state = Global.GAME_STATE.GAME_OVER


func _set_score(new_score: int):
	score = new_score
	$GUI.set_score(score)
	
func on_update_score(points: int):
	_set_score(score + points)
	

func _unhandled_input(event):
	if Global.game_state != Global.GAME_STATE.GAME_OVER:
		return
		
	if event.is_action_pressed("ui_shoot"):
		_restart_game()
			

func _restart_game():
	Global.game_state = Global.GAME_STATE.PLAYING
	#var screen_size = get_viewport_rect().size
	var player = player_scene.instance()
	
	player.global_position = Vector2(160, 160) #screen_size / 2
	add_child(player)
	
	_set_score(0)
	$GUI.set_high_score(Global.high_score)
	$GUI.on_game_start()
	$AsteroidSpawner.restart()
